FROM nginxinc/nginx-unprivileged:latest

EXPOSE 8081

COPY dist/prancing-ponies/* /usr/share/nginx/html/
