import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators'
import { environment } from 'src/environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class Trade {

apiUrl: string = environment.rest_host

  constructor(private http: HttpClient) {
  }

  // method to display error message
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
      return throwError('something bad happened, please try again later')

    };
  }

  // method to retrieve trade data from backend
  getData() {
    return this.http.get(`${this.apiUrl}/trade`).pipe(catchError(this.handleError<Object[]>('getData', []))
    );
  }

  // method to retrieve data on stocks from backend for search
  getSearchData(trade)
  {
    return this.http.get(`${this.apiUrl}/trade`)
    .pipe(catchError(this.handleError<Object[]>('getParamData', []))
    );
  }
  
  // method to retrieve stock data from backend
  getStockData() {
    return this.http.get(`${this.apiUrl}/stock`).pipe(catchError(this.handleError<Object[]>('getData', []))
    );
  }

  // commented out method to send user inpu from ui to the backend
  // getTmaStrategyData(inputStock, inputPrice, inputVolume, maximumPrice, maximumTime, maximumVolume)
   //{
    // return this.http.get(`${this.apiUrl}/trade${inputStock}/${inputPrice}/${inputVolume}/${maximumVolume}
     //${maximumPrice}/${maximumTime}`)
     //.pipe(catchError(this.handleError<Object[]>('postTmaStrategyData', []))
    //);
   //}


 

}


