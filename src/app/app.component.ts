import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Trade } from './trade.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'prancing-ponies';

// setting parameters for methods
inputPrice:number = 1
inputVolume:number=666
inputStock:string="AEF"
maximumPrice
maximumVolume
maximumTime
model
strategy
stock
search
whichTrade:string = 'AllTrades'
choices:Object[] = [{cat:'AllTrades'},{cat:'GOOG'},{cat:'AMZN'},{cat:'EBAY'},{cat:'MCD'}];

// importing service from trade class
constructor(private trade: Trade) {
}

ngOnInit() {
}

// method to call invoke service method when user clicks show trades
handleClick()
{
  this.invokeService();
}

// calling the getData method from trade service class
invokeService() {
  this.trade.getData().subscribe(
    (result) => {
      console.log(result);
      this.model = result;
    })
}

// commented out method for search button
// handleClick()
//   {
//     this.trade.getSearchData( this.whichTrade)
//     .subscribe( (result)=>{this.search = result})
//   }


// method to call invokeStockService method when user clicks show stocks
handleStockClick()
{
  this.invokeStockService();
}

// calling getStockData method from trade service class
invokeStockService() {
  this.trade.getStockData().subscribe(
    (result) => {
      console.log(result);
      this.stock = result;
    })
}

// commented out method for getting user input from ui and sending to backend
//handleSubmit()
//{
  //this.trade.getTmaStrategyData(this.inputStock, this.inputPrice, this.inputVolume, this.maximumPrice,
    //this.maximumVolume, this.maximumTime )
  //.subscribe( (result)=>{
    //    console.log(result);
      //  this.strategy = result})
//}


}