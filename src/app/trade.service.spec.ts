import { TestBed } from '@angular/core/testing';

import { Trade } from './trade.service';

describe('Trade', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: Trade = TestBed.get(Trade);
    expect(service).toBeTruthy();
  });
});
